      program transaction
      use pdi
      implicit none

      type(pc_tree_t),target :: conf
      integer, target        :: my_int
      real, target           :: my_float
      character, target      :: my_string(32)
      integer, pointer       :: p_my_int
      real, pointer          :: p_my_float
      character, pointer     :: p_my_string(:)

      call pc_parse_path("hello_multi_expose.yml", conf)
      call pdi_init(conf)

      my_int = 0
      p_my_int => my_int
      my_float = 0
      p_my_float => my_float
      my_string = "RGB = Really Gawky Biscuit";
      p_my_string => my_string

      call pdi_transaction_begin("event_between")

      call pdi_expose("an_int", p_my_int, pdi_out)
      call pdi_expose("a_float", p_my_float, pdi_out)
      call pdi_expose("a_string", p_my_string, pdi_out)

      call pdi_transaction_end()

      call pdi_finalize()
      call pc_tree_destroy(conf)

      end program transaction
