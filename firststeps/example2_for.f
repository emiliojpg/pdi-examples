      program event
      use paraconf
      use pdi
      implicit none

      type(pc_tree_t),target :: conf
      integer, target        :: my_world
      integer, pointer       :: p_my_world

      call pc_parse_path("hello_data.yml", conf)
      call pdi_init(conf)

      my_world = 42
      p_my_world => my_world
      call pdi_share("world", p_my_world, pdi_out)

      call pdi_reclaim("world")

      call pdi_finalize()
      call pc_tree_destroy(conf)

      end program event
