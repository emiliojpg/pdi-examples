#include <pdi.h>

void print_secret_msg() {
  int *the_value;
  PDI_access("value", (void **) &the_value, PDI_IN);
  printf("PDI value: %d\n", *the_value);
  PDI_release("value");

  char *the_message;
  PDI_access("message", (void **) &the_message, PDI_IN);
  printf("PDI message: %s\n", the_message);
  PDI_release("message");
}

int main(int argc, char* argv[]){
  PC_tree_t conf = PC_parse_path("hello_access.yml");
  PDI_init(conf);

  int my_value = 42;
  PDI_share("value", &my_value, PDI_OUT);

  char *secret_message = "Orange is the tastiest fruit";
  PDI_share("message", secret_message, PDI_OUT);

  printf("My value: %d\n", my_value);
  printf("My message: %s\n", secret_message);

  print_secret_msg();

  PDI_reclaim("message");
  PDI_reclaim("value");

  PDI_finalize();
  PC_tree_destroy(&conf);

  return 0;
}
