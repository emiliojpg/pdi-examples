### Simple example from [First steps with PDI](https://pdi.julien-bigot.fr/master/First_steps.html)

A simple makefile that assumes PDI in /opt/pdi. If PDI is installed in
standard paths in a FHS such as /usr or /usr/local then probably
`LDLIBS` would be enough.

This makefile has not target at all. An explicit target must be
specified like this:

`make example`

This compiles and links an executable binary from a source file
`example.c` or `example.f` using the appropriate compiler.

Idem with:

`make example_for`

To execute the generated binary you need the PDI shared libs in a PATH
the dynamic linker can find.

A quick workaround can be something like this:

`LD_LIBRARY_PATH=/opt/pdi/lib ./example`

Or permanently add that path to the paths the dynamic linker knows.
In my Debian box I can do that by adding a
`/etc/ld.so.conf.d/pdi.conf` file with that path, like this:

    % cat /etc/ld.so.conf.d/pdi.con
    #pdi in /opt/pdi
    /opt/pdi/lib

Basically the makefile helps the compiler to find the header files
(`pdi.h` in C, `paraconf.mod` and `pdi.mod` in fortran) and tells the
linker to link against the libs (`libpdi` and `libparaconf`).
