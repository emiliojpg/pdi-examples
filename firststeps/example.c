#include <pdi.h>

int main(int argc, char* argv[]) {

  // 1. Assign the tree: PC_tree_t conf = PC_parse_path(...);
  PC_tree_t conf = PC_parse_path("hello_event.yml");

  // 2. Pass tree to PDI: PDI_init(conf);
  PDI_init(conf);

  PDI_event("Hello World Event");

  // 3. Finalize PDI: PDI_finalize()
  PDI_finalize();

  // 4. Destroy the tree at the end: PC_tree_destroy(&conf);
  PC_tree_destroy(&conf);

  return 0;
}
