      subroutine print_secret_msg
      use pdi
      implicit none

      integer, pointer               :: p_val
      character, pointer, contiguous :: p_msg(:)
      integer                        :: msg_ranks(1)

      call pdi_access("value", p_val, pdi_in)
      print *, "PDI value: ", p_val
      call pdi_release("value")

      msg_ranks(1) = 28
      call PDI_access("message", p_msg, pdi_in, msg_ranks)
      print *, "PDI message: ", p_msg
      call pdi_release("message")

      return
      end

      program access
      use paraconf
      use pdi
      implicit none

      type(pc_tree_t),target          :: conf
      integer, target                 :: my_value
      integer, pointer                :: p_my_value
      character, target               :: secret_message(28)
      character, pointer, contiguous  :: p_secret_message(:)

      call pc_parse_path("hello_access.yml", conf)
      call pdi_init(conf)

      my_value = 42
      p_my_value => my_value
      call pdi_share("value", p_my_value, pdi_out)

      secret_message = (/ "O", "r", "a", "n", "g", "e", " ", "i", "s",
     $     " ", "t", "h", "e", " ", "t", "a", "s", "t", "i", "e", "s",
     $     "t", " ", "f", "r", "u", "i", "t" /)
      p_secret_message => secret_message
      call pdi_share("message", p_secret_message, pdi_out)

      print *, "My value: ", my_value
      print *, "My message: ", secret_message

      call print_secret_msg()

      call pdi_reclaim("message")
      call pdi_reclaim("value")

      call pdi_finalize()
      call pc_tree_destroy(conf)

      end program access
