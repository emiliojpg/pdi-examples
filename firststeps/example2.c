#include <pdi.h>

int main(int argc, char* argv[]) {
  PC_tree_t conf = PC_parse_path("hello_data.yml");
  PDI_init(conf);

  int my_world = 42;

  PDI_share("world", &my_world, PDI_OUT);
  //variable my_world is shared with PDI

  PDI_reclaim("world");
  //variable my_world is no longer shared with PDI

  PDI_finalize();
  PC_tree_destroy(&conf);

  return 0;
}
