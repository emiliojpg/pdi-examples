      program event
      use paraconf
      use pdi
      implicit none

      type(pc_tree_t),target :: conf

      call pc_parse_path("hello_event.yml", conf)
      call pdi_init(conf)

      call pdi_event("Hello World Event")

      call pdi_finalize()

      call pc_tree_destroy(conf)

      end program event
