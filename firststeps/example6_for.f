      program loop
      use pdi
      implicit none

      type(pc_tree_t),target :: conf
      integer, target        :: my_int
      real, target           :: my_float
      integer, pointer       :: p_my_int
      real, pointer          :: p_my_float
      integer                :: my_loop = 100000

      call pc_parse_path("example6.yml", conf)
      call pdi_init(conf)

      my_int = 0
      p_my_int => my_int
      my_float = 0
      p_my_float => my_float

      do while (my_loop > 0)
         call pdi_transaction_begin("event_between")

         call pdi_expose("an_int", p_my_int, pdi_out)
         call pdi_expose("a_float", p_my_float, pdi_out)

         call pdi_transaction_end()

         my_int = my_int + 1
         my_float = my_float + 0.1
         my_loop = my_loop - 1;
      end do

      call pdi_finalize()
      call pc_tree_destroy(conf)

      end program loop
