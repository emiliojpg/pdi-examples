      program event
      use paraconf
      use pdi
      implicit none

      type(pc_tree_t),target :: conf
      integer                :: my_world

      call pc_parse_path("pycall_ondata.yml", conf)
      call pdi_init(conf)

      my_world = 42

      print *, "Fortran value: ", my_world

c$$$      call pdi_share("world", p_my_world, pdi_out)
c$$$      call pdi_reclaim("world")
      call pdi_expose("world", my_world, pdi_out)

      call pdi_finalize()
      call pc_tree_destroy(conf)

      end program event
