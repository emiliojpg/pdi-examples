#include <pdi.h>

int main(int argc, char* argv[]) {
  PC_tree_t conf = PC_parse_path("example6.yml");
  PDI_init(conf);

  int my_int = 0;
  float my_float = 0;
  int my_loop = 100000;

  while (my_loop > 0) {
    PDI_multi_expose("event_between",
                     "an_int", &my_int, PDI_OUT,
                     "a_float", &my_float, PDI_OUT,
                     NULL);
    my_int += 1;
    my_float += 0.1;
    my_loop -= 1;
  }

  PDI_finalize();
  PC_tree_destroy(&conf);

  return 0;
}
