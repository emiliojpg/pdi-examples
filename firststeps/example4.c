#include <pdi.h>

int main(int argc, char* argv[]) {
  PC_tree_t conf = PC_parse_path("hello_multi_expose.yml");
  PDI_init(conf);

  int my_int = 0;
  float my_float = 0;
  char* my_string = "RGB = Really Gawky Biscuit";

  PDI_multi_expose("event_between",
                   "an_int", &my_int, PDI_OUT,
                   "a_float", &my_float, PDI_OUT,
                   "a_string", &my_string, PDI_OUT,
                   NULL);

  PDI_finalize();
  PC_tree_destroy(&conf);

  return 0;
}
