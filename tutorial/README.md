### PDI tutorial from [Hands-on tutorial](https://pdi.julien-bigot.fr/master/Hands_on.html)

The solution to each of the 10 exercises are here with this README, ready
to compile and execute, whereas the material to follow the tutorial is in
the subdirectory `tasks_stuff`.

A variant of ex3, `ex3_alt`, has been added to test Pycall plugin to
access data from Python once is shared (`ex10` shows the use of the
Pycall plugin when an event is called).

A brief explanation about the example code can be seen in [PDI
examples](https://pdi.julien-bigot.fr/master/PDI_example.html)

The makefile assumes PDI in /opt/pdi. If PDI is installed in standard
paths in a FHS such as /usr or /usr/local then probably `LDLIBS` would
be enough. MPI is assumed in standard path (e.g. /usr or /usr/local).

A `CMakeLists.txt`file is in `tasks_stuff` if you prefer
to use `cmake` instead of this simple makefile.

This makefile has not target at all. An explicit target must be
specified like this:

    make ex1

This compiles and links an executable binary from a source file
`ex1.c` using the appropriate compiler (`mpicc` by default).

To execute the generated binary you need the PDI shared libs in a PATH
the dynamic linker can find, and use `mpirun` with the appropriate
number of mpi ranks. For example:

+ Sequential run, just one MPI process, as `ex1.c` uses `ex1.yml`,
with parallelism set up to 1x1:

        $ make ex1
        $ mpirun -n 1 ./ex1

+ Four MPI processes, with parallelism 2x2 in ex1_2x2.yml:

        $ make ex1_2x2
        $ mpirun -n 4 ./ex1_2x2

+ Two MPI processes, with parallelism 2x1 in ex4.yml:

        $ make ex4
        $ mpirun -n 2 ./ex4

The last example (`ex10`) uses Python to post-process data through the
PDI module Pycall. In order to access the PDI module in Python you need
to have it in some of the standards paths where the Python interpreter
looks for modules or use the `PYTHONPATH` env variable like this:

    $ make ex10
    $ PYTHONPATH=/opt/pdi/lib/python3/dist-packages mpirun -n 4 ./ex10

Of course, the same is needed for `ex3_alt`:

    $ make ex3_alt
    $ PYTHONPATH=/opt/pdi/lib/python3/dist-packages mpirun -n 1 ./ex3_alt
